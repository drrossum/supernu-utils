SuperNu Simulation Output Plotting Utilities
============================================

A SuperNu run generates output files named `output.*` in a scratch directory `myrun/`, say.

Spectra
---
	# plspnSpec.py myrun/ --time 20

Bolometric luminosity light curves
---
	# plspnLum.py myrun/

Photometric band filter light curves
---
	# plspnBand.py myrun/

Philips Relation
---
	# plspnPhil.py myrun/
