#!/usr/bin/python2
import argparse
import sys
import os.path
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import itertools
from matplotlib.ticker import AutoMinorLocator

import snlib.supernu
import snlib.physconst as pc
import snlib.daan

linestyles = ["-","--","-.",":"]
markers = [',', '+', '-', '.', 'o', '*']

bandcolor = dict(zip(['0','U','B','V','R','I','J','H','K'], ['k','m','b','g','r','orange','c','y','.5']))

#-- input args
ext = '.ext' #{{{
#-- config file
cfname = ''
args = []
for arg in sys.argv[1:]:
  if arg[0] == '@': cfname = arg[1:]
  else: args.append(arg)
#-- read config file
if cfname != '':
  with open(cfname, 'r') as f:
    cfargs = f.readlines()
#-- save input arguments to file
prog = sys.argv[0]
cfname = 'argparse.' + os.path.basename(prog)[:-3]
with open(cfname, 'w') as f:
  if 'cfargs' in locals(): f.writelines(cfargs)
  if len(args) > 0: f.writelines('\n'.join(args) + '\n')


#-- setup parser
parser = argparse.ArgumentParser(fromfile_prefix_chars='@', description='')
parser.add_argument('--names',nargs='+', help='Names for input files')
parser.add_argument('--xlim', nargs=2, type=float, help='Time plot range')
parser.add_argument('--ylim', nargs=2, type=float, help='Magnitude plot range')
parser.add_argument('--xlog', action='store_true', help='Logarithmic x-axis')
parser.add_argument('--ylog', action='store_true', help='Logarithmic y-axis')

parser.add_argument('--band', nargs='+', type=str, default=list('UBVRI'), choices=list('0UBVRIJHK'), help='filter bands to plot')

parser.add_argument('--rebin',  choices=['mu','phi','pi4'], help='integrate over [mu,phi,pi4] viewing angle')
parser.add_argument('--imu',  nargs='+', type=int, help='viewing angles to plot')
parser.add_argument('--iphi', nargs='+', type=int, help='viewing angles to plot')

parser.add_argument('--showshapepar', action='store_true', help='plot lc shape parameters')

parser.add_argument('--figsize', nargs=2, type=float, default=[8,6], help='Figure size')
parser.add_argument('--eps',   dest='figext', action='store_const', default='.pdf', const='.eps', help='Figure file type is EPS')
parser.add_argument('--fnout', type=str, help='figure filename')
parser.add_argument('files',   nargs='+', help='Input data files')
#-- parse arguments
args = parser.parse_args()
#-- default names
if args.names is not None:
  assert len(args.names) == len(args.files)
  #-- treat _ as 'space' character
  if not any(' ' in s for s in args.names):
    args.names = [s.replace('_',' ') for s in args.names]

#-- prepend path
print 'numer of input files:', len(args.files)

#-- output file name
if args.fnout is None:
  args.fnout = os.path.abspath(args.files[0])
  if args.fnout[-1] == '/': args.fnout += 'output'  #-- directory with trailing /
  else: args.fnout += '/output'                       #-- directory w/o trailing /
  if len(args.files) > 1: args.fnout += '+'
#}}}

fdata = []
for i,fname in enumerate(args.files):
  f = snlib.supernu.PostProc(fname + '/')
  #-- rebin viewing angle
  f.rebin_viewingangle(args.rebin)
  f.rebin_viewingangle_mag(args.rebin)

  if args.names is not None: f.name = args.names[i]
  if f.name[-4:] == '.out': f.name = f.name[:-4]
  for b in args.band: assert b in f.band
  fdata.append(f)

fnouts = []

fig = plt.figure(figsize=args.figsize)#{{{

#-- ylim
if args.ylim is not None:#{{{
  ylim = args.ylim
else:
  ax = fig.add_subplot(111)

  for i,f in enumerate(fdata):
    slc = snlib.daan.lim2slc(f.time[:-1]/pc.day, (10,64))
    if slc.stop - slc.start <= 1: continue
    for iphi,phi in enumerate(f.phi):
      for imu,mu in enumerate(f.mu):
        for b in args.band:
          iib = f.band.index(b)
          ax.plot(f.time[slc]/pc.day, f.magnitude[iib,iphi,imu,slc])

  ylim = ax.get_ylim()
  ylim = [ylim[0]+3, ylim[0]]
  fig.delaxes(ax)
#}}}

ax = fig.add_subplot(111)
linecycle = itertools.cycle(linestyles)
labels = []
for i,f in enumerate(fdata):
  label = ['','','']
  label[0] = f.name
  for iphi,phi in enumerate(f.phi):
    if len(f.phi) > 1: label[1] = 'phi={:.2f}'.format(phi)
    if args.iphi is not None  and  not iphi in args.iphi: continue
    for imu,mu in enumerate(f.mu):
      if len(f.mu) > 1: label[2] = 'mu={:.2f}'.format(mu)
      if args.imu is not None  and  not imu in args.imu: continue
      linestyle = next(linecycle)
      for ib,b in enumerate(args.band):
        if ib == 0:
          labl = ', '.join([s for s in label[1:] if not s in ['',' ']])
          labels.append(labl)
          labl = ', '.join([s for s in label if not s in ['',' ']])
        else: labl = ''
        color = bandcolor[b] #if len(args.band) > 1 else 'k'
        iib = f.band.index(b)
        ax.plot(f.time/pc.day, f.magnitude[iib,iphi,imu], linestyle, label=labl, color=color)
        if args.showshapepar:
          print b, f.magshape[iib,iphi,imu,0]/pc.day, f.magshape[iib,iphi,imu,1]
          ax.plot(f.magshape[iib,iphi,imu,0]/pc.day, f.magshape[iib,iphi,imu,1], marker='+', color='k')
          ax.plot(f.magshape[iib,iphi,imu,0]/pc.day + 15, f.magshape[iib,iphi,imu,1:].sum(), marker='+', color='k')

if len(fdata) > 1:
  leg1handles = [Line2D([], [], color='k', linestyle=(10*linestyles)[i], label=f.name) for i,f in enumerate(fdata)]
else:
  leg1handles = [Line2D([], [], color='k', linestyle=(10*linestyles)[i], label=s) for i,s in enumerate(labels)]
leg2handles = [Line2D([], [], color=bandcolor[b], label=b) for b in args.band]


#-- name legend
leg1 = ax.legend(handles=leg1handles, frameon=False, labelspacing=0)
#-- band legend
if len(args.band) > 1: 
  ax.legend(handles=leg2handles, frameon=False, loc='upper left')
  plt.gca().add_artist(leg1)

ax.set_ylim(ylim)
if args.xlog: ax.set_xscale('log')
if args.xlim is not None: ax.set_xlim(args.xlim)
ax.set_ylabel('Absolute Magnitude')
ax.set_xlabel('Time post explosion [day]')
if not args.ylog: ax.yaxis.set_minor_locator(AutoMinorLocator())
if not args.xlog: ax.xaxis.set_minor_locator(AutoMinorLocator())

fig.tight_layout()

fnout = args.fnout + '.Band' + args.figext
fnouts.append(fnout)
print 'saving:', fnout
fig.savefig(fnout)
#}}}

# vim: ts=2 sts=2 sw=2 et fdm=marker
