#!/usr/bin/python2
import argparse
import sys
import os.path

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.ticker import AutoMinorLocator
import itertools
import copy

import snlib.supernu as supernu
import snlib.physconst as pc
import snlib.daan as daan

colors = 3*['b', 'g', 'r', 'c', 'm', 'y', 'k', 'orange', 'pink']
linestyles = 3*['-', '--', ':', '-.']

#-- input args
ext = '.ext' #{{{
#-- config file
cfname = ''
args = []
for arg in sys.argv[1:]:
  if arg[0] == '@': cfname = arg[1:]
  else: args.append(arg)
#-- read config file
if cfname != '':
  with open(cfname, 'r') as f:
    cfargs = f.readlines()
#-- save input arguments to file
prog = sys.argv[0]
cfname = 'argparse.' + os.path.basename(prog)[:-3]
with open(cfname, 'w') as f:
  if 'cfargs' in locals(): f.writelines(cfargs)
  if len(args) > 0: f.writelines('\n'.join(args) + '\n')


#-- setup parser
parser = argparse.ArgumentParser(fromfile_prefix_chars='@', description='')
parser.add_argument('-a',  action='store_true', help='plot all quantities')

parser.add_argument('--names',nargs='+', help='Names for input files')
parser.add_argument('--xlim', nargs=2, type=float, help='Wavelength plot range')
parser.add_argument('--ylim', nargs=2, type=float, help='Magnitude plot range')
parser.add_argument('--xlog', action='store_true', help='Logarithmic x-axis')
parser.add_argument('--ylin', action='store_true', help='plot erg/s instead of magnitudes')

parser.add_argument('--rebin',  choices=['mu','phi','pi4'], help='integrate over [mu,phi,pi4] viewing angle')
parser.add_argument('--imu',  nargs='+', type=int, help='viewing angles to plot')
parser.add_argument('--iphi', nargs='+', type=int, help='viewing angles to plot')

parser.add_argument('--title', type=str, help='plot title')

parser.add_argument('--figsize', nargs=2, type=float, default=[8,6], help='Figure size')
parser.add_argument('--eps',   dest='figext', action='store_const', default='.pdf', const='.eps', help='Figure file type is EPS')
parser.add_argument('--fnout','-o', type=str, help='figure filename')
parser.add_argument('files',   nargs='+', help='Input data files')
#-- parse arguments
args = parser.parse_args()
#-- default names
if args.names is not None:
  assert len(args.names) == len(args.files)
  #-- treat _ as 'space' character
  if not any(' ' in s for s in args.names):
    args.names = [s.replace('_',' ') for s in args.names]

print 'numer of input files:', len(args.files)

#-- output file name
if args.fnout is None:
  args.fnout = os.path.abspath(args.files[0])
  if args.fnout[-1] == '/': args.fnout += 'postproc'  #-- directory with trailing /
  else: args.fnout += '/postproc'                       #-- directory w/o trailing /
  if len(args.files) > 1: args.fnout += '+'

#}}}

fdata = []
for i,fname in enumerate(args.files):
  f = supernu.PostProc(fname + '/')
  if args.a: f.readflux()
#-- rebin viewing angle
  f.rebin_viewingangle(args.rebin)
  f.rebin_viewingangle_mag(args.rebin)

  if args.names is not None: f.name = args.names[i]
  if f.name[-4:] == '.out': f.name = f.name[:-4]
  fdata.append(f)


if True:
  class dummy: pass
  g = dummy()
  g.name = 't^2'
  g.mu = [0]
  g.phi = [np.pi]
  g.dmu = [2]
  g.dphi = [2*np.pi]
  l0, t0 = 3e29, 1.6*pc.day
  g.time = np.linspace(t0,100*pc.day,1000)
  g.lum = l0 * (g.time - t0)**2
  g.lum = g.lum.reshape([1,1,-1,1]) #-- phi,mu,tim,wl
  mag0lum = 3.011e+35
  g.magnitude = -2.5*np.log10(g.lum/mag0lum).reshape([1,1,1,-1]) #-- band,phi,mu,tim
  fdata.append(g)


fnouts = []

#-- multiple angles for any model
nangle = []
for i,f in enumerate(fdata):
  #-- mu
  nmu = 1
  if args.imu is None:
    if len(f.mu) > 1: nmu = f.nmu
  elif len(args.imu) > 1: nmu = len(args.imu)
  #-- phi
  nphi = 1
  if args.iphi is None:
    if len(f.phi) > 1: nphi = f.nphi
  elif len(args.iphi) > 1: nphi = len(args.nphi)
  f.nangle = nmu*nphi
  nangle.append(nmu*nphi)


#-- Lum
fig = plt.figure(figsize=args.figsize)#{{{

##-- ylim
#if args.ylim is not None:#{{{
#  ylim = args.ylim
#else:
#  ax = fig.add_subplot(111)
#
#  ylim = None #-- default
#  for i,f in enumerate(fdata):
#    slc = daan.lim2slc(f.time[:-1], np.array([10,64])*pc.day)
#    if slc.stop - slc.start <= 1: continue
#    for iphi,phi in enumerate(f.phi):
#      for imu,mu in enumerate(f.mu):
#        if args.ylin: y = f.lum[iphi,imu].sum(axis=1)
#        else: y = f.magnitude[0,iphi,imu]
#        ax.plot(f.time[slc]/pc.day, y[slc])
#    #-- evaluate result
#    ylim = ax.get_ylim()
#    ylim = [ylim[0]+3, ylim[0]]
#
#  fig.delaxes(ax)
##}}}

#-- final plot with all data
ax = fig.add_subplot(111)

handles = []
labels = []
for i,f in enumerate(fdata):
  label = ['','','']
  label[0] = f.name
  slc = daan.lim2slc(f.time[:-1]/pc.day, args.xlim)

  #-- line color and style
  if i == 0  or  max(nangle)>1:
    linestyle = linestyles[i]
    colorcycle = itertools.cycle(colors)

  for imu,mu in enumerate(f.mu):
    if len(f.mu) > 1: label[1] = 'mu={:.2f}'.format(mu)
    if args.imu is not None  and  not imu in args.imu: continue
    for iphi,phi in enumerate(f.phi):
      if len(f.phi) > 1: label[2] = r'phi={:.2f}$\pi$'.format(phi/pc.pi)
      if args.iphi is not None  and  not iphi in args.iphi: continue

      labl = ', '.join([s for s in label if not s in ['',' ']])

      if args.ylin: y = f.lum[iphi,imu].sum(axis=1)
      else: y = f.magnitude[0,iphi,imu]
      lin, = ax.plot(f.time[slc]/pc.day, y[slc], label=labl, linestyle=linestyle, color=colorcycle.next())
      #ax.plot(f.magshape['0'][iphi,imu,0]/pc.day, f.magshape['0'][iphi,imu,1], marker='+', color='k')

      #-- save plot handles
      labels.append(label[:])
      handles.append(copy.copy(lin))

def add_legend():
  if max(nangle)==1  or  len(fdata) == 1:
    ax.legend()
  else:
    if len(fdata) == 1:
      labls = [', '.join([s for s in l if not s in ['',' ']]) for l in labels]
      ax.legend(handles[:sum(nangle)], labls, loc='best', title=fdata[0].name)
    else:
      labls = [', '.join([s for s in l[1:] if not s in ['',' ']]) for l in labels]
      leg1 = ax.legend(handles[:sum(nangle)], labls, loc='best')
      handlesk = [copy.copy(h) for h in handles]
      [h.set_color('k') for h in handlesk]
      idx = np.array([0] + nangle[:-1]).cumsum()
      ax.legend([handlesk[i] for i in idx], [f.name for f in fdata], loc='upper left')
      plt.gca().add_artist(leg1)

add_legend()

ylim = ax.get_ylim()
if args.ylim is not None: ylim = args.ylim
elif args.ylin: ylim = ylim[::-1]
ax.set_ylim(ylim)

if args.ylin: ax.set_ylabel('Bolometric Luminosity [erg/s]')
else: ax.set_ylabel('Bolometric Magnitude')

if args.xlog: ax.set_xscale('log')
if args.xlim is not None: ax.set_xlim(args.xlim)
ax.set_xlabel('Time (post explosion) [day]')
ax.yaxis.set_minor_locator(AutoMinorLocator())
if not args.xlog: ax.xaxis.set_minor_locator(AutoMinorLocator())

if args.title is not None: fig.suptitle(args.title, y=.993)
fig.tight_layout()

fnout = args.fnout + '.Lum' + args.figext
fnouts.append(fnout)
print 'saving:', fnout
fig.savefig(fnout)
#}}}


if not args.a: sys.exit()


#-- Num
fig = plt.figure(figsize=args.figsize)#{{{
ax = fig.add_subplot(111)

for i,f in enumerate(fdata):
  label = ['','','']
  label[0] = f.name

  #-- line color and style
  if i == 0  or  max(nangle)>1:
    linestyle = linestyles[i]
    colorcycle = itertools.cycle(colors)

  if not 'lumnum' in dir(f): continue
  for iphi,phi in enumerate(f.phi):
    if len(f.phi) > 1: label[1] = 'phi={:.2f}'.format(phi)
    if args.iphi is not None  and  not iphi in args.iphi: continue
    for imu,mu in enumerate(f.mu):
      if len(f.mu) > 1: label[2] = 'mu={:.2f}'.format(mu)
      if args.imu is not None  and  not imu in args.imu: continue
      labl = ', '.join([s for s in label if not s in ['',' ']])
      lumnumtot = f.lumnum.sum(axis=-1)
      ax.plot(f.time/pc.day, lumnumtot[iphi,imu], label=labl, linestyle=linestyle, color=colorcycle.next())

add_legend()

if args.xlog: ax.set_xscale('log')
else: ax.xaxis.set_minor_locator(AutoMinorLocator())
if args.ylin: ax.yaxis.set_minor_locator(AutoMinorLocator())
else: ax.set_yscale('log')
if args.xlim is not None: ax.set_xlim(args.xlim)
ax.set_ylabel('Number of Flux Particles')
ax.set_xlabel('Time post explosion [day]')

if args.title is not None: fig.suptitle(args.title, y=.993)
fig.tight_layout()

fnout = args.fnout + '.Num' + args.figext
fnouts.append(fnout)
print 'saving:', fnout
fig.savefig(fnout)
#}}}

#-- mean particle energy
fig = plt.figure(figsize=args.figsize)#{{{
ax = fig.add_subplot(111)

for i,f in enumerate(fdata):
  label = ['','','']
  label[0] = f.name

  #-- line color and style
  if i == 0  or  max(nangle)>1:
    linestyle = linestyles[i]
    colorcycle = itertools.cycle(colors)

  if not 'lumnum' in dir(f): continue
  for iphi,phi in enumerate(f.phi):
    if len(f.phi) > 1: label[1] = 'phi={:.2f}'.format(phi)
    if args.iphi is not None  and  not iphi in args.iphi: continue
    for imu,mu in enumerate(f.mu):
      if len(f.mu) > 1: label[2] = 'mu={:.2f}'.format(mu)
      if args.imu is not None  and  not imu in args.imu: continue
      labl = ', '.join([s for s in label if not s in ['',' ']])
      y = f.lum[iphi,imu].sum(axis=-1) / f.lumnum[iphi,imu].sum(axis=-1)
      ax.plot(f.time/pc.day, y, label=labl, linestyle=linestyle, color=colorcycle.next())

add_legend()

if args.xlog: ax.set_xscale('log')
if args.xlim is not None: ax.set_xlim(args.xlim)
ax.set_ylabel('Mean Particles energy [erg/s]')
ax.set_xlabel('Time post explosion [day]')
ax.yaxis.set_minor_locator(AutoMinorLocator())
if not args.xlog: ax.xaxis.set_minor_locator(AutoMinorLocator())

if args.title is not None: fig.suptitle(args.title, y=.993)
fig.tight_layout()

fnout = args.fnout + '.LumOverNum' + args.figext
fnouts.append(fnout)
print 'saving:', fnout
fig.savefig(fnout)
#}}}

#-- gamLum
fig = plt.figure(figsize=args.figsize)#{{{
ax = fig.add_subplot(111)

for i,f in enumerate(fdata):
  label = ['','','']
  label[0] = f.name
  slc = daan.lim2slc(f.time[:-1]/pc.day, args.xlim)

  #-- line color and style
  if i == 0  or  max(nangle)>1:
    linestyle = linestyles[i]
    colorcycle = itertools.cycle(colors)

  if not 'gamlum' in dir(f): continue
  for iphi,phi in enumerate(f.phi):
    if len(f.phi) > 1: label[1] = 'phi={:.2f}'.format(phi)
    if args.iphi is not None  and  not iphi in args.iphi: continue
    for imu,mu in enumerate(f.mu):
      if len(f.mu) > 1: label[2] = 'mu={:.2f}'.format(mu)
      if args.imu is not None  and  not imu in args.imu: continue
      labl = ', '.join([s for s in label if not s in ['',' ']])
      y = pc.pi4/(f.dmu[imu]*f.dphi[iphi])*f.gamlum[iphi,imu]
      ax.plot(f.time[slc]/pc.day, y[slc], label=labl, linestyle=linestyle, color=colorcycle.next())

add_legend()

ax.set_yscale('log')
ylim = list(ax.get_ylim())
ylim[0] = max(ylim[0], ylim[1]*1e-25)
ax.set_ylim(ylim)
if args.xlog: ax.set_xscale('log')
if args.xlim is not None: ax.set_xlim(args.xlim)
ax.set_ylabel('Gamma Luminosity [erg/s/ster]')
ax.set_xlabel('Time post explosion [day]')
if not args.xlog: ax.xaxis.set_minor_locator(AutoMinorLocator())

if args.title is not None: fig.suptitle(args.title, y=.993)
fig.tight_layout()

fnout = args.fnout + '.gamLum' + args.figext
fnouts.append(fnout)
print 'saving:', fnout
fig.savefig(fnout)
#}}}

# vim: ts=2 sts=2 sw=2 et fdm=marker
