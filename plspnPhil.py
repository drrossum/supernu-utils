#!/usr/bin/python2
import argparse
import sys
import os.path
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator

import snlib.supernu as supernu
import snlib.physconst as pc
import snlib.snobs as snobs

colors = 2*['b','g','r','c','m','y','orange','pink','.55']
linestyles = 2*["-","--","-.",":"]

#-- input args
ext = '.ext' #{{{
#-- config file
cfname = ''
args = []
for arg in sys.argv[1:]:
  if arg[0] == '@': cfname = arg[1:]
  else: args.append(arg)
#-- read config file
if cfname != '':
  with open(cfname, 'r') as f:
    cfargs = f.readlines()
#-- save input arguments to file
prog = sys.argv[0]
cfname = 'argparse.' + os.path.basename(prog)[:-3]
with open(cfname, 'w') as f:
  if 'cfargs' in locals(): f.writelines(cfargs)
  if len(args) > 0: f.writelines('\n'.join(args) + '\n')


#-- setup parser
parser = argparse.ArgumentParser(fromfile_prefix_chars='@', description='')
parser.add_argument('--names',nargs='+', help='Names for input files')
parser.add_argument('--group',nargs='+', type=int, help='Names for input files')
parser.add_argument('--xlim', nargs=2, type=float, default=[.5, 2.], help='Time plot range')
parser.add_argument('--ylim', nargs=2, type=float, default=[-17.5, -20.1], help='Magnitude plot range')

parser.add_argument('--band', default='B', help='LC band in which brightness and decline rate are measured')
parser.add_argument('--nodata',action='store_true', help='Do not plot observational data points')

parser.add_argument('--rebin',  choices=['mu','phi','pi4'], help='integrate over [mu,phi,pi4] viewing angle')
parser.add_argument('--imu', nargs='+', type=int, help='viewing angles to plot')
parser.add_argument('--iphi', nargs='+', type=int, help='viewing angles to plot')

parser.add_argument('--figsize', nargs=2, type=float, default=[8,6], help='Figure size')
parser.add_argument('--eps',   dest='figext', action='store_const', default='.pdf', const='.eps', help='Figure file type is EPS')
parser.add_argument('--fnout','-o', type=str, help='figure filename')
parser.add_argument('files',   nargs='+', help='Input data files')
#-- parse arguments
args = parser.parse_args()
#-- default names
if args.names is not None:
  assert len(args.names) == len(args.files)
  #-- treat _ as 'space' character
  if not any(' ' in s for s in args.names):
    args.names = [s.replace('_',' ') for s in args.names]

if args.group is not None:
  assert sum(args.group) == len(args.files)

print 'numer of input files:', len(args.files)

#-- output file name
if args.fnout is None:
  args.fnout = os.path.abspath(args.files[0])
  if args.fnout[-1] == '/': args.fnout += 'output'  #-- directory with trailing /
  else: args.fnout += '/output'                       #-- directory w/o trailing /
  if len(args.files) > 1: args.fnout += '+'
#}}}

fdata = []
for i,fname in enumerate(args.files):
  f = supernu.PostProc(fname + '/')
#-- rebin viewing angle
  f.rebin_viewingangle_mag(args.rebin)

  if args.names is not None: f.name = args.names[i]
  if f.name[-4:] == '.out': f.name = f.name[:-4]
  fdata.append(f)

fnouts = []

fig = plt.figure(figsize=args.figsize)#{{{
ax = fig.add_subplot(111)

if args.group is not None: group = args.group
else: group = [9,9,9,9]
colors = [colors[i] for j in range(len(group)) for i in range(group[j])]
linestyles = [linestyles[j] for j in range(len(group)) for i in range(group[j])]



#-- phillips relation
if args.band == 'B': snobs.phillips = snobs.phillips_b
elif args.band == 'V': snobs.phillips = snobs.phillips_v
elif args.band == 'I': snobs.phillips = snobs.phillips_i
if args.band == 'B': snobs.phillips_sigma = snobs.phillips_b_sigma
elif args.band == 'V': snobs.phillips_sigma = snobs.phillips_v_sigma
elif args.band == 'I': snobs.phillips_sigma = snobs.phillips_i_sigma
#x = np.linspace(args.xlim[0], args.xlim[1], 100)
x = np.linspace(.85, 1.7, 100)
y = snobs.phillips(x)
lin, = ax.plot(x, y, label='Phillips Relation', color='k')
x = np.linspace(.85, 1.7, 100)
y = snobs.phillips(x)
y_up = y - snobs.phillips_sigma
y_dn = y + snobs.phillips_sigma
ax.fill_between(x, y_dn, y_up, edgecolor='None', facecolor='.9')


#-- data points
for i,f in enumerate(fdata):
  label = f.name
  ib = f.band.index('B')
  iib = f.band.index(args.band)
  lin, = ax.plot(f.magshape[ib,0,:,2], f.magshape[iib,0,:,1], color=colors[i], ls=linestyles[i], label=label)
  if f.nmu > 1: ax.scatter(f.magshape[ib,:,:,2], f.magshape[iib,:,:,1], c=f.mu, marker='+', s=200, cmap=plt.cm.coolwarm_r)
  #if f.nmu > 1: ax.scatter(f.magshape[ib,:,:,2], f.magshape[iib,:,:,1], c=f.mu, marker='+', s=200, cmap=plt.cm.gist_rainbow)
  else: ax.scatter(f.magshape[ib,:,:,2], f.magshape[iib,:,:,1], color=lin.get_color(), marker='+', s=200)
  #ax.scatter(f.magshape[ib,:,1:-1,2], f.magshape[iib,:,1:-1,1], marker='+', color=colors[i], s=100)
  #ax.scatter(f.magshape[ib,:,-1,2], f.magshape[iib,:,-1,1], marker='^', color=colors[i], s=100)
  #ax.scatter(f.magshape[ib,:,0,2], f.magshape[iib,:,0,1], marker='v', color=colors[i], s=100)

#-- individual SNe
if not args.nodata:
  x = [0.94, 0.9, 0.63, 1.05]
  y = [-19.87, -19.94, -20.02, -19.41]
  ytext = np.array(y) + [.02, -.01, 0, 0]
  label = ['1991T', '2003fg', '2009dc', '2011fe']
  ax.plot(x, y, 'k', linestyle='', marker='*', markersize=10)
  for i,s in enumerate(label):
    ax.text(x[i]+.03, ytext[i], s, verticalalignment='center', fontsize=10)


ax.legend()

ax.set_xlim(args.xlim)
ax.set_ylim(args.ylim)
band = args.band
if band == '0': band = 'bol'
ax.set_ylabel(r'M(' + band + ')')
ax.set_xlabel(r'$\Delta M_{15}(' + band + ')$')
ax.yaxis.set_minor_locator(AutoMinorLocator())
ax.xaxis.set_minor_locator(AutoMinorLocator())

fig.tight_layout()

fnout = args.fnout + '.Phil' + args.figext
fnouts.append(fnout)
print 'saving:', fnout
fig.savefig(fnout)
#}}}

# vim: ts=2 sts=2 sw=2 et fdm=marker
