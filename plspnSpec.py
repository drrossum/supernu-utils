#!/usr/bin/python2
import argparse
import sys
import os.path

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator
from matplotlib.lines import Line2D
import itertools

import snlib.supernu as supernu
import snlib.physconst as pc
import snlib.daan as daan

colors = 3*['b', 'g', 'r', 'c', 'm', 'y', 'k', 'orange', 'pink']
linestyles = 3*['-', '--', ':', '-.', '--.']

#-- input args
ext = '.ext' #{{{
#-- config file
cfname = ''
args = []
for arg in sys.argv[1:]:
  if arg[0] == '@': cfname = arg[1:]
  else: args.append(arg)
#-- read config file
if cfname != '':
  with open(cfname, 'r') as f:
    cfargs = f.readlines()
#-- save input arguments to file
prog = sys.argv[0]
cfname = 'argparse.' + os.path.basename(prog)[:-3]
with open(cfname, 'w') as f:
  if 'cfargs' in locals(): f.writelines(cfargs)
  if len(args) > 0: f.writelines('\n'.join(args) + '\n')


#-- setup parser
parser = argparse.ArgumentParser(fromfile_prefix_chars='@', description='')
parser.add_argument('-a',  action='store_true', help='plot all quantities')

parser.add_argument('--names',nargs='+', help='Names for input files')
parser.add_argument('--time', nargs='+', required=True, type=float, help='Time post explosion for which to plot the spectra')
parser.add_argument('--xlim', nargs=2, type=float, help='Wavelength plot range')
parser.add_argument('--ylim', nargs=2, type=float, help='Magnitude plot range')
parser.add_argument('--xlog', action='store_true', help='Logarithmic x-axis')
parser.add_argument('--ylog', action='store_true', help='Logarithmic y-axis')

parser.add_argument('--rebin',  choices=['mu','phi','pi4'], help='integrate over [mu,phi,pi4] viewing angle')
parser.add_argument('--imu', nargs='+', type=int, help='viewing angles to plot')
parser.add_argument('--iphi', nargs='+', type=int, help='viewing angles to plot')

parser.add_argument('--stddev', action='store_true', help='do not plot the standard deviation')

parser.add_argument('--title', type=str, help='plot title')

parser.add_argument('--figsize', nargs=2, type=float, default=[8,6], help='Figure size')
parser.add_argument('--eps',   dest='figext', action='store_const', default='.pdf', const='.eps', help='Figure file type is EPS')
parser.add_argument('--fnout','-o', type=str, help='output filename')
parser.add_argument('files',   nargs='+', help='Input data files')
#-- parse arguments
args = parser.parse_args()

#-- default names
if args.names is not None:
  assert len(args.names) == len(args.files)
  #-- treat _ as 'space' character
  if not any(' ' in s for s in args.names):
    args.names = [s.replace('_',' ') for s in args.names]

#-- default title
if args.title is None  and  len(args.time) == 1: args.title = 'Day ' + (len(args.time)*'{:3.1f} ').format(*tuple(args.time))

#-- prepend path
print 'numer of input files:', len(args.files)

#-- output file name
if args.fnout is None:
  args.fnout = os.path.abspath(args.files[0])
  if args.fnout[-1] == '/': args.fnout += 'output'  #-- directory with trailing /
  else: args.fnout += '/output'                       #-- directory w/o trailing /
  if len(args.files) > 1: args.fnout += '+'
#}}}

class Hsiao07():
  def __nonzero__(self): return False

fdata = []
for i,fname in enumerate(args.files):
  f = supernu.Output(fname + '/')
  f.readflux()
#-- rebin viewing angle
  f.rebin_viewingangle(args.rebin)
  #-- select one spectrum
  f.itime = np.searchsorted(f.time, pc.day*np.array(args.time))
  print f.time[f.itime]/pc.day

  with np.errstate(invalid='ignore'): f.lumdev /= f.lum
  if args.names is not None: f.name = args.names[i]
  fdata.append(f)

fnouts = []

#-- print flux norm info
if not all(fdata):
  for i,f in enumerate(fdata):
    if isinstance(f, Hsiao07):
      norm0 = (f.flux * daan.dwl(f.wl)).sum()
  for i,f in enumerate(fdata):
    if isinstance(f, Hsiao07): continue
    for i,it in enumerate(f.itime):
      norm = (f.flux[:,:,it] * daan.dwl(f.wl)).sum()
      print norm, norm/norm0


#-- number of angles to be plotted
nangle = []
for i,f in enumerate(fdata):
  #-- mu
  nmu = 1
  if args.imu is None:
    if len(f.mu) > 1: nmu = f.nmu
  elif len(args.imu) > 1: nmu = len(args.imu)
  #-- phi
  nphi = 1
  if args.iphi is None:
    if len(f.phi) > 1: nphi = f.nphi
  elif len(args.iphi) > 1: nphi = len(args.nphi)
  f.nangle = nmu*nphi
  nangle.append(nmu*nphi)


#-- Spec
fig = plt.figure(figsize=args.figsize)#{{{
ax = fig.add_subplot(111)

colorcycle = itertools.cycle(colors)
linecycle = itertools.cycle(linestyles)
for l,f in enumerate(fdata):
  label = ['','','']
  label[0] = f.name

  slc = daan.lim2slc(f.wl/pc.ang, args.xlim)

  #-- line color
  if max(nangle) == 1  and  len(f.itime) == 1:
    if l == 0: ls = linecycle.next()
    color = colorcycle.next()
  else:
    ls = linecycle.next()
    colorcycle = itertools.cycle(colors)

  for k,phi in enumerate(f.phi):
    if len(f.phi) > 1: label[1] = 'phi={:.2f}'.format(phi)
    if args.iphi is not None  and  not k in args.iphi: continue
    for j,mu in enumerate(f.mu):
      if len(f.mu) > 1: label[2] = 'mu={:.2f}'.format(mu)
      if args.imu is not None  and  not j in args.imu: continue

      for i,it in enumerate(f.itime):
        #-- line color
        if nphi*nmu > 1  or  len(f.itime) > 1: color = next(colorcycle)

        labl = ', '.join([s for s in label if not s in ['',' ']])
        if i != 0: labl = ''
        if isinstance(f, Hsiao07):
          ax.plot(f.wl[slc]/pc.ang, f.flux[k,j,0][slc], '--', color='k', label=label[0])
        else:
          lin, = ax.plot(f.wl[slc]/pc.ang, f.flux[k,j,it][slc], ls, label=labl, color=color)
        if args.stddev  and  'lumdev' in dir(f):
          yup = (1 + f.lumdev[k,j,it])*f.flux[k,j,it]
          ydn = (1 - f.lumdev[k,j,it])*f.flux[k,j,it]
          #-- smooth
          #yup[1:-1] = .25*(2*yup[1:-1] + yup[:-2] + yup[2:])
          #ydn[1:-1] = .25*(2*ydn[1:-1] + ydn[:-2] + ydn[2:])
          ax.fill_between(f.wl[slc]/pc.ang, yup[slc], ydn[slc], color=color, alpha=.4)

#-- second legend
if len(args.time) == 1:
  leg1 = ax.legend()
else:
  #-- leg1
  linecycle = itertools.cycle(linestyles)
  leglines = [Line2D([], [], color='k', ls=next(linecycle)) for t in args.time]
  leglabels = [f.name for f in fdata]
  leg1 = ax.legend(leglines, leglabels)
  #-- leg2
  colorcycle = itertools.cycle(colors)
  leglines = [Line2D([], [], color=next(colorcycle)) for t in args.time]
  leglabels = ['Day {:.1f}'.format(t) for t in args.time]
  ax.legend(leglines, leglabels, loc='upper left')
  #-- add back leg1
  plt.gca().add_artist(leg1)

if args.ylim is not None: ax.set_ylim(args.ylim)
if args.xlim is not None: ax.set_xlim(args.xlim)
if args.ylog: ax.set_yscale('log')
if args.xlog: ax.set_xscale('log')
ax.set_ylabel('Flux [erg/s/cm/ster]')
ax.set_xlabel('Wavelength [ang]')
if not args.ylog: ax.yaxis.set_minor_locator(AutoMinorLocator())
if not args.xlog: ax.xaxis.set_minor_locator(AutoMinorLocator())

if args.title is not None: fig.suptitle(args.title, y=.993)
fig.tight_layout()

fnout = args.fnout + '.Spec' + args.figext
fnouts.append(fnout)
print 'saving:', fnout
fig.savefig(fnout)
#}}}


if not args.a: sys.exit()


#-- specnum
fig = plt.figure(figsize=args.figsize)#{{{
ax = fig.add_subplot(111)

colorcycle = itertools.cycle(colors)
linecycle = itertools.cycle(linestyles)
for l,f in enumerate(fdata):
  label = ['','','']
  label[0] = f.name

  slc = daan.lim2slc(f.wl/pc.ang, args.xlim)

  #-- line color
  if len(f.phi) * len(f.mu) == 1  and  len(f.itime) == 1: color = colorcycle.next()
  else:
    ls = linecycle.next()
    colorcycle = itertools.cycle(colors)

  if not 'lumnum' in dir(f): continue
  for k,phi in enumerate(f.phi):
    if len(f.phi) > 1: label[1] = 'phi={:.2f}'.format(phi)
    if args.iphi is not None  and  not k in args.iphi: continue
    for j,mu in enumerate(f.mu):
      if len(f.mu) > 1: label[2] = 'mu={:.2f}'.format(mu)
      if args.imu is not None  and  not j in args.imu: continue
      for i,it in enumerate(f.itime):
        #-- line color
        if len(f.phi) * len(f.mu) > 1  or  len(f.itime) > 1: color = next(colorcycle)
        labl = ', '.join([s for s in label if not s in ['',' ']])
        if i != 0: labl = ''
        ax.plot(f.wl[slc]/pc.ang, f.lumnum[k,j,it][slc], ls, color=color, label=labl)

leg1 = ax.legend()
if len(args.time) > 1:
  ax.legend(leglines, leglabels, frameon=False, loc='upper left')
  plt.gca().add_artist(leg1)
if args.xlim is not None: ax.set_xlim(args.xlim)
if args.xlog: ax.set_xscale('log')
if args.ylog: ax.set_yscale('log')
ax.set_ylabel('Number flux [1/bin]')
ax.set_xlabel('Wavelength [ang]')
if not args.xlog: ax.xaxis.set_minor_locator(AutoMinorLocator())

if args.title is not None: fig.suptitle(args.title, y=.993)
fig.tight_layout()

fnout = args.fnout + '.SpecNum' + args.figext
fnouts.append(fnout)
print 'saving:', fnout
fig.savefig(fnout)
#}}}

#-- specdev
fig = plt.figure(figsize=args.figsize)#{{{
ax = fig.add_subplot(111)

colorcycle = itertools.cycle(colors)
linecycle = itertools.cycle(linestyles)
for l,f in enumerate(fdata):
  label = ['','','']
  label[0] = f.name

  slc = daan.lim2slc(f.wl/pc.ang, args.xlim)

  #-- line color
  if len(f.phi) * len(f.mu) == 1  and  len(f.itime) == 1: color = colorcycle.next()
  else:
    ls = linecycle.next()
    colorcycle = itertools.cycle(colors)

  if not 'lumdev' in dir(f): continue
  for k,phi in enumerate(f.phi):
    if len(f.phi) > 1: label[1] = 'phi={:.2f}'.format(phi)
    if args.iphi is not None  and  not k in args.iphi: continue
    for j,mu in enumerate(f.mu):
      if len(f.mu) > 1: label[2] = 'mu={:.2f}'.format(mu)
      if args.imu is not None  and  not j in args.imu: continue
      for i,it in enumerate(f.itime):
        #-- line color
        if len(f.phi) * len(f.mu) > 1  or  len(f.itime) > 1: color = next(colorcycle)
        labl = ', '.join([s for s in label if not s in ['',' ']])
        if i != 0: labl = ''
        ax.plot(f.wl[slc]/pc.ang, f.lumdev[k,j,it][slc], ls, color=color, label=labl)

leg1 = ax.legend()
if len(args.time) > 1:
  ax.legend(leglines, leglabels, frameon=False, loc='upper left')
  plt.gca().add_artist(leg1)
if args.xlim is not None: ax.set_xlim(args.xlim)
if args.xlog: ax.set_xscale('log')
if args.ylog: ax.set_yscale('log')
ax.set_yscale('log')
#ylim = ax.get_ylim()
#ymax = min(ylim[1], ylim[0]*1e2)
#ax.set_ylim(top=ymax)
#ylim = ax.get_ylim()
#ax.set_ylim(top=min(ylim[1],.1))
ax.set_ylim(1e-2, 1)
ax.set_ylabel('Relative Flux Error')
ax.set_xlabel('Wavelength [ang]')
if not args.xlog: ax.xaxis.set_minor_locator(AutoMinorLocator())

if args.title is not None: fig.suptitle(args.title, y=.993)
fig.tight_layout()

fnout = args.fnout + '.SpecDev' + args.figext
fnouts.append(fnout)
print 'saving:', fnout
fig.savefig(fnout)
#}}}

# vim: ts=2 sts=2 sw=2 et fdm=marker
