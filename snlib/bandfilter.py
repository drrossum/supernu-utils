import numpy as np
#from scipy import interpolate
from collections import OrderedDict

import physconst as pc
import daan

filtercolor = OrderedDict(zip(['0','U','B','V','R','I','J','H','K'], ['k','m','b','g','r','y','c','orange','.5']))


class Filter:
  '''Astro observational band filters.'''
  def __init__(self, fn, bands):#{{{
    self.fn = fn
    self.zeropoints()
    self.bands = list(bands)
    self.read()
    self.read_all()

  def zeropoints(self):
    #-- constants#{{{
    m0lband = ['0', 'U', 'B', 'V', 'R', 'I', 'J', 'H', 'K']
    #
    #-- dkasen zero points for peak normalized Bessell90 filters
    #mag0lum = np.array([2.5852824e+35, 4.9035743e+31, 7.4809295e+31, 4.3057651e+31, 2.5841726e+31, 1.3467185e+31, 1., 1., 1.]) #Bessell90, peak normalized
    #
    #-- Bessell98 zero points for area normalized Bessell90 filters
    #mag0lum = np.array([2.5852824e+35, 4.9970910e+31, 7.5634034e+31, 4.3442745e+31, 2.6056571e+31, 1.3474642e+31, 3.7663204e+30, 1.3611853e+30, 4.7371513e+29])

    zpt = -21.100 - np.array([-0.152, -0.602,  0.000,  0.555,  1.271,  2.655,  3.760,  4.906]) #1998A&A...333..231B
    mag0lum = np.pi*4.*(10.*pc.parsec)**2 * 10.**(zpt/2.5)
    #-- bolometric
    #mag0lum = np.append(2.5852824e+35, mag0lum)
    mag0lum = np.append(3.011e+35, mag0lum)  #-- assuming Mbolsun=4.74 and Lbolsun=3.826e+33

    self.mag0lum = dict(zip(m0lband, mag0lum))
#}}}

  def read(self):
    #-- read one file per band filter#{{{
    self.nbin = np.zeros(len(self.bands), dtype=int)
    for i,x in enumerate(self.bands):
      fn = self.fn.replace('?',x)
      data = np.loadtxt(fn)
      #-- store length per band
      self.nbin[i] = len(data)
      #-- concatenate filter to previous ones
      if i == 0:
        filters = data
      else:
        filters = np.concatenate((filters, data))
    #-- split filters
    data = np.zeros([len(self.bands), self.nbin.max(), 2])
    k = 0
    for j in np.arange(len(self.bands)):
      data[j,:self.nbin[j],:] = filters[k:k+self.nbin[j], :]
      k += self.nbin[j]
    self.wl2lwl(data)
#}}}

  def wl2lwl(self, data):
    #-- boundaries of the wavelength bins#{{{
    self.nbin -= 2
    self.lwl = np.zeros([len(self.bands), self.nbin.max()+1])
    for j in np.arange(len(self.bands)):
      for i in np.arange(self.nbin[j]+1):
        self.lwl[j, i] = .5*(data[j, i:i+2, 0].sum())
    #...self.wl = data[:, 1:-1, 0]
    self.trns = data[:, 1:-1, 1]
    trans_int = np.sum(self.trns*(self.lwl[:,1:] - self.lwl[:,:-1]), axis=1)
    for j in np.arange(len(self.bands)):
      self.trns[j, :] /= trans_int[j]
#}}}

  def read_all(self):
    self.filter = {}#{{{
    for x in self.bands:
      fn = self.fn.replace('?', x)
      data = np.loadtxt(fn).transpose()

      #-- normalize
      dwl = daan.dwl(data[0, :])
      norm = 1./(data[1, :]*dwl).sum()
      data[1, :] *= norm

      #-- save
      self.filter[x] = data
#}}}

  def convolve(self, wl, flux):
    '''Fold spectrum through passband filters, return the filter response.'''
    response = {}#{{{
    for k,x in self.filter.iteritems():
      #-- interpolate filter onto spec grid
      filter = np.interp(wl, x[0,:], x[1,:])
      #f = interpolate.interp1d(x[0,:], x[1,:], fill_value=0, bounds_error=False)
      #filter = f(wl)
      resp = (flux*filter*daan.dwl(pc.ang*wl)).sum(axis=-1)
      response[k] = resp
    return response
#}}}

  def mag(self, response):
    '''Convert filter response to magnitudes.'''
    magnitude = {}#{{{
    for k,x in response.iteritems():
      mag = -2.5 * np.log10(x / self.mag0lum[k])
      magnitude[k] = mag
      #print k,mag
    return magnitude
#}}}
#}}}

# vim: ts=2 sts=2 sw=2 et fdm=marker
