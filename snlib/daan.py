#!/usr/bin/python2
import numpy as np
import scipy.interpolate
from scipy import optimize


def dwl(wl):
  '''Return wl-bin widths.'''
  dwl = wl.copy()#{{{
  dwl[1:-1] = .5*(wl[2:] - wl[:-2])
  dwl[0] = wl[1] - wl[0]
  dwl[-1] = wl[-1] - wl[-2]
  return dwl
#}}}

def lim2slc(x, xlim):
  '''create an array slice from an array plus a min,max tuple'''
  if xlim is None: return slice(None)
  elif len(xlim) != 2: raise Exception('wrong xlim shape')#{{{
  return slice( *tuple(np.searchsorted(x, xlim) + [0,1]))
#}}}

def seq2slcs(lst, iterator=False):
   '''Turn a list with sequences into a list of slices.'''
   #-- beginpoints of sequences#{{{
   iseq = []
   lprev = None
   for i,l in enumerate(lst):
     if l - 1 != lprev: iseq.append(i)
     lprev = l
   #-- number of sequences
   nseq = len(iseq)

   #-- once slice per sequence
   seq = []
   seqiter = []
   for i in range(nseq-1):
     seq.append(slice(lst[iseq[i]], lst[iseq[i+1]-1]+1))
     seqiter.append(slice(iseq[i], iseq[i+1]))
   #-- add endpoint
   seq.append(slice(lst[iseq[-1]], lst[-1]+1))
   seqiter.append(slice(iseq[-1], len(lst)))

   if iterator: return seq, seqiter
   else: return seq
#}}}

def extrap1d(interpolator):
  '''Turn an interp1d function into a extrapolatable function'''
  xs = interpolator.x#{{{
  ys = interpolator.y

  def pointwise(x):
    if x > xs[0]:
      return ys[0]+(x-xs[0])*(ys[1]-ys[0])/(xs[1]-xs[0])
    elif x < xs[-1]:
      return ys[-1]+(x-xs[-1])*(ys[-1]-ys[-2])/(xs[-1]-xs[-2])
    else:
      return interpolator(x)

  def ufunclike(xs):
    return np.array(map(pointwise, np.array(xs)))

  return ufunclike
#}}}

def fit_flux(wlref, fluxref, fluxreferr, wl, flux, window=None):
  '''Fit fluxes on different wl grids'''
  fluxfit = np.interp(wlref, wl, flux)#{{{
  #-- error function
  if window == None: err = fluxreferr
  else:
    slc = lim2slc(wlref, window)
    err = 1e300*np.ones_like(fluxreferr)
    err[slc] = fluxreferr[slc]

  func = lambda x: (fluxref - x*fluxfit) / err
  par0 = 1.
  par, success = optimize.leastsq(func, par0)
  norm = par[0]
  #-- formal chi2
  func = lambda x: (fluxref - x*fluxfit) / fluxreferr
  chi2 = (func(norm)**2).mean()
  return norm, chi2
#}}}


def specstack(wl, flux, igroup=None, xlim=None, hmargin=7, vmargin=0, reverse=False):
  '''Calculate offsets that stack spectra (flux list) without intersecting'''
  #-- default group IDs#{{{
  if igroup == None: igroup = [i for i in len(flux)]
  #-- number of groups
  ngroup = len(set(igroup))

  #-- quick exit
  if ngroup == 1: return np.array([0, flux[0].max()])

  if reverse:
    igroup = igroup[::-1]

  slc = lim2slc(wl[0], xlim)
  wlref = wl[0][slc]
  nwl = len(wlref)

  #-- init min/max arrays
  fluxmin = 1e300*np.ones([ngroup, nwl])
  fluxmax = np.zeros([ngroup, nwl])

  #-- determine offsets
  for i,ig in enumerate(igroup):
    lumfunc = scipy.interpolate.interp1d(wl[i], flux[i], kind=1, bounds_error=False)
    lum = lumfunc(wlref)
    fluxmax[ig] = np.fmax(fluxmax[ig], lum)
    fluxmin[ig] = np.fmin(fluxmin[ig], lum)

  #-- up-down margin
  fluxmax = vmargin*fluxmax

  #-- include left-right margin
  di = hmargin
  if di != 0:
    fluxmin[:, di:-di] = np.fmin(fluxmin[:, di:-di], fluxmin[:, 2*di:])
    fluxmin[:, di:-di] = np.fmin(fluxmin[:, di:-di], fluxmin[:, :-2*di])
    fluxmax[:, di:-di] = np.fmax(fluxmax[:, di:-di], fluxmax[:, 2*di:])
    fluxmax[:, di:-di] = np.fmax(fluxmax[:, di:-di], fluxmax[:, :-2*di])

  #-- calculate offset (last group first)
  offsetlist = np.zeros([ngroup+1])
  for i in range(1,ngroup):
    doffset = (fluxmax[i-1] - fluxmin[i]).max()
    offsetnew = offsetlist[i-1] + doffset
    offsetlist[i] = offsetnew

  #-- snap to next whole integer/snap for visibility
  offsetmax = offsetlist.max()
  p10 = 10**np.floor(np.log10(offsetmax))
  if offsetmax/p10 < 5: p10 /= 5
  elif offsetmax/p10 < 2: p10 /= 2

  for i in range(1,ngroup):
    offsetnew = np.ceil(offsetlist[i]/p10) * p10
    #-- shift next curves along with this curve's new vertical position
    if i < ngroup-1: offsetlist[i+1:] += offsetnew - offsetlist[i]
    offsetlist[i] = offsetnew
    #-- at least one snap distance away
    #if i > 0: offsetlist[i] = max(offsetlist[i]/p10, offsetlist[i+1]/p10 + 1)

  #-- ymax
  offsetlist[-1] = fluxmax[-1].max() + offsetlist[-2]

  if reverse: offsetlist[:-1] = offsetlist[:-1][::-1]

  #print offsetlist

  return offsetlist
#}}}


def convolve_gauss_lin(xin, yin, wdth, npoints=10):
  '''Convolve array "a" with a gaussian smoothing function of width "wdth"'''
  #-- create log wl grid on which is interpolated#{{{
  x0, x1 = xin[0], xin[-1]
  xint = np.arange(x0, x1 + wdth/npoints, wdth/npoints)
  xint[-1] = x1  #-- extend to full wl range for interpolation
  #-- interpolate integrated function to account for strong variability between interpolation points
  dxin = np.diff(xin)
  dxin = np.hstack(([dxin[0]], dxin))
  ycum = np.cumsum(yin*dxin)
  func = scipy.interpolate.interp1d(xin, ycum)
  ycumint = func(xint)
  ycumint = np.hstack((ycumint, np.array([ycumint[-1]])))
  yint = np.diff(ycumint)
  dxint = np.diff(xint).mean()
  yint /= dxint
  #-- smooth
  xf = np.linspace(-3, 3, 6*npoints)
  func = np.exp(-xf**2)
  yconv = convolve_normalized(yint, func)
  func = scipy.interpolate.interp1d(xint, yconv)
  yout = func(xin)
  return yout
#}}}

def convolve_normalized(a, f):
  '''Convolve array "a" with smoothing function "f" and maintain normalization at the edges'''
  norm = np.ones([len(a)])#{{{
  norm = 1/np.convolve(norm, f, mode='same')
  b = norm*np.convolve(a, f, mode='same')
  return b
#}}}

class LabeledColumns():
  '''Generic data file format'''
  def __init__(self, fname, strlen=None):#{{{
    self.fname = fname
    self.strlen = strlen
    self.readfile()

  def readfile(self):
    with open(self.fname, 'r') as f:
      labl = f.readline()[1:].split()
      datapos = f.tell()
      self.data = np.genfromtxt(f).transpose()
      #-- reread file if it contains char columns
      if self.strlen != None:
        f.seek(datapos) #-- rewind
        data_str = np.genfromtxt(f, dtype='S{:}'.format(self.strlen)).transpose()
    #-- put data in dict
    self.dat = {}
    for i,var in enumerate(labl):
      if np.isnan(self.data[i,:]).all(): self.dat[var] = data_str[i]
      else: self.dat[var] = self.data[i]
#}}}

# vim: ts=2 sts=2 sw=2 et fdm=marker
