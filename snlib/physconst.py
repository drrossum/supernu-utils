import numpy as np

#-- physical constants in cgs units
pi = 3.1415926535897932385
pi2 = 2.*pi
pi4 = 4.*pi

c = 2.997924562e10 #cm/s
g = 6.6742e-8
rg = 8.314472e7
kb = 1.3806505e-16 #erg/K
me = 9.1093826e-28 #g
mh = 1.67333e-24 #g
amu = 1.66053886e-24 #g
h = 6.6260693e-27 #erg*s
e = 4.80325e-10 #sqrt(erg*cm)
ev = 1.60217653e-12 #erg
abohr = 0.5291772108e-8 #cm
rydberg = 1.0973731534e5 #cm^-1

sb = 2*pi**5*kb**4/(15.*h**3*c**2) #5.670400e-5 #erg/cm^2/s/K^4
hsun = 3.826e33/pi4**2 #erg/s/cm^2/sr
msun = 1.989e33 #g
rsun = 6.96342e8 #cm
au = 1.4960e13 #cm

parsec = 3.0857e18 #cm

#-- conversion factors
day = 24*60.**2
year = 31556925.9747 #sec
km = 1e5 #cm
ang = 1e-8 #cm
barn = 1e-24 #cm^2
mbarn = 1e-18 #cm^2 (Mega barn)

#-- cosmology constants
hubbleconst = 72*km/1e6 #(per parsec)
omega_r = 5e-5
omega_m = .27
w_lambda = -1.

#-- magnitude zero points
mag0lum = 3.01e35

def planck(wl, temp):
  """Blackbody intensity on wavelength [cm] grid.  Units: erg/cm^2/s/cm/ster
  Note: the flux from a blackbody is pi*B_l(T), in units erg/cm^2/s/cm."""
  c1 = 2.*h*c**2#{{{
  c2 = h*c/kb
#-- standard form
# b = wl
# b = 2.*h*c**2/(b**5*np.expm1(h*c/(b*kb*temp))) #units: erg/cm^2/s/cm/ster
#-- optimized form
  b = 1./(wl*temp)
  b = c1*(temp*b)**5/np.expm1(c2*b) #units: erg/cm^2/s/cm/ster
  return b
#}}}

# vim: ts=2 sts=2 sw=2 et fdm=marker
