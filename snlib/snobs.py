#!/usr/bin/python2
import numpy as np
import os.path
import sys

import physconst as pc

class Hsiao():
  def __init__(self, fname, fluxfactor=1.):#{{{
    self.fname = fname
    self.fluxfactor = fluxfactor
    self.read()

  def read(self):
    print 'reading:', self.fname
    with open(self.fname,'r') as f:
      f.readline()
      pars = f.readline().replace('#','').split()
      self.nwl, self.nday, day1, dayn = tuple([int(x) for x in pars])
      data = np.loadtxt(f).reshape([self.nday+1, self.nwl])
      self.wl = data[0,:]
      self.flux = data[1:,:] * self.fluxfactor
      self.day = range(day1,dayn+1)#}}}


class Mlcs():
  def __init__(self, fname, h0=72.):#{{{
    self.fname = fname
    self.h0 = h0
    self.read()

  def read(self):
    bands = 'UBVRI'
    self.data = {}
    print 'loading:', self.fname
    for b in bands:
      fname = self.fname.replace('?', b)
      self.data[b] = self.readone(fname)
      self.data[b][0,:] += 5.*np.log10(self.h0/65.)

  def readone(self, fname):
    with open(fname,'r') as f:
      f.readline()
      data = np.loadtxt(f).transpose()
      self.day = data[0, :]
      return data[1:, :]

  def lc(self, band, delta):
    vec = delta**np.array([0, 1, 2])
    return (vec*self.data[band].transpose()).sum(axis=1) #}}}

#-- phillips relation, 1999AJ....118.1766P;  offset (-19.xx) eyeballed from their figures
phillips_b = lambda dm15b: -19.55 + 5*np.log10(72/65.) + .786 * (dm15b - 1.1) + .633 * (dm15b - 1.1)**2
phillips_v = lambda dm15b: -19.45 + 5*np.log10(72/65.) + .672 * (dm15b - 1.1) + .633 * (dm15b - 1.1)**2
phillips_i = lambda dm15b: -19.15 + 5*np.log10(72/65.) + .422 * (dm15b - 1.1) + .633 * (dm15b - 1.1)**2
phillips_b_sigma = .11
phillips_v_sigma = .09
phillips_i_sigma = .13

# vim: ts=2 sts=2 sw=2 et fdm=marker
